#ifndef WIDGET_H
#define WIDGET_H

#include <QWidget>
#include <QFile>
#include <QJSEngine>
#include <QDebug>

QT_BEGIN_NAMESPACE
namespace Ui { class Widget; }
QT_END_NAMESPACE

class Widget : public QWidget
{
    Q_OBJECT

public:
    Widget(QWidget *parent = nullptr);
    ~Widget();

private slots:
    void on_pushButton_LatLongElev2XYZ_clicked();

    void on_pushButton_XYZ2LatLongElev_clicked();

    void on_pushButton_ClearLatLongElev_clicked();

    void on_pushButton_ClearXYZ_clicked();

private:
    Ui::Widget *ui;
    QJSEngine myEngine;
    bool isJavaScriptFileLoaded;
};
#endif // WIDGET_H

#include "widget.h"
#include "ui_widget.h"

Widget::Widget(QWidget *parent)
    : QWidget(parent)
    , ui(new Ui::Widget)
{
    ui->setupUi(this);
    isJavaScriptFileLoaded = false;
    QString fileName = ":/resources/geodesy.js";
    QFile scriptFile(fileName);
    if (!scriptFile.open(QIODevice::ReadOnly))
    {
        qDebug() << "could not open javascript file.";
    }
    else
    {
        QTextStream stream(&scriptFile);
        QString contents = stream.readAll();
        scriptFile.close();
        myEngine.evaluate(contents, fileName);
        isJavaScriptFileLoaded = true;
        qDebug() << "javascript file loaded.";
    }
    ui->lineEdit_Lat->setText("-15.869167");
    ui->lineEdit_Long->setText("-47.920833");
    ui->lineEdit_Elev->setText("1066.000000");
}

Widget::~Widget()
{
    delete ui;
}

void Widget::on_pushButton_LatLongElev2XYZ_clicked()
{
    if(isJavaScriptFileLoaded)
    {
        // convert lat, long, height to x, y, z
        QString script = "";
        script += "var vec_xyz;";
        double elevation = ui->lineEdit_Elev->text().toDouble();
        elevation = elevation / 1000;
        script += "vec_xyz = llhxyz(" + ui->lineEdit_Lat->text() + "," + ui->lineEdit_Long->text() + ", " + QString::number(elevation, 'f', 6) + ");";
        qDebug() << "Script: " << script;
        QJSValue result = myEngine.evaluate(script);
        QStringList numbersList;

        if (result.isError())
        {
           qDebug() << "Uncaught exception at line" << result.property("lineNumber").toInt() << ":" << result.toString();
           return;
        }
        else
        {
            if (result.isArray())
            {
                qDebug() << "Resul is array.";
                double X, Y, Z;
                int length = result.property("length").toInt();
                qDebug() << "Length:" << length;
                for(int i = 0; i < length; i++)
                {
                    QString number;
                    number = result.property(i).toString();
                    numbersList.push_back(number);
                    switch (i)
                    {
                    case 0:
                        qDebug() << "x = " << numbersList[0];
                        X = numbersList[0].toDouble();
                        ui->lineEdit_X->setText(QString::number(X, 'f', 6));
                        break;
                    case 1:
                        qDebug() << "y = " << numbersList[1];
                        Y = numbersList[1].toDouble();
                        ui->lineEdit_Y->setText(QString::number(Y, 'f', 6));
                        break;
                    case 2:
                        qDebug() << "z = " << numbersList[2];
                        Z = numbersList[2].toDouble();
                        ui->lineEdit_Z->setText(QString::number(Z, 'f', 6));
                        break;
                    }
                }
           }
           qDebug() <<  result.toString();
        }
    }
}


void Widget::on_pushButton_XYZ2LatLongElev_clicked()
{
    if(isJavaScriptFileLoaded)
    {
        // convert x, y, z to lat, long, height
        QString script = "";
        QStringList numbersList;
        script = "";
        script += "var  xyz_vec = new Array(3);";
        script += "xyz_vec[0] = " + ui->lineEdit_X->text() + ";";
        script += "xyz_vec[1] = " + ui->lineEdit_Y->text() + ";";
        script += "xyz_vec[2] = " + ui->lineEdit_Z->text() + ";";
        script += "test_LatLongHeight = xyzllh(xyz_vec);";
        qDebug() << "Script: " << script;

        QJSValue result = myEngine.evaluate(script);
        result = myEngine.evaluate(script);
        if (result.isError())
        {
           qDebug() << "Uncaught exception at line" << result.property("lineNumber").toInt() << ":" << result.toString();
           return;
        }
        else
        {
           if (result.isArray())
           {
               qDebug() << "Resul is array.";
               double latitude, longitude, elevation;
               int length = result.property("length").toInt();
               qDebug() << "Length:" << length;
               numbersList.clear();
               for(int i = 0; i < length; i++)
               {
                   QString number;
                   number = result.property(i).toString();
                   numbersList.push_back(number);
                   switch (i)
                   {
                   case 0:
                       qDebug() << "lat = " << numbersList[0];
                       latitude = numbersList[0].toDouble();
                       ui->lineEdit_Lat->setText(QString::number(latitude, 'f', 6));
                       break;
                   case 1:
                       qDebug() << "long = " << numbersList[1];
                       longitude = numbersList[1].toDouble();
                       ui->lineEdit_Long->setText(QString::number(longitude, 'f', 6));
                       break;
                   case 2:
                       qDebug() << "altitude = " << numbersList[2];
                       elevation = numbersList[2].toDouble();
                       elevation = elevation * 1000;
                       ui->lineEdit_Elev->setText(QString::number(elevation, 'f', 6));
                       break;
                   }
               }
           }
           qDebug() <<  result.toString();
        }
    }
}


void Widget::on_pushButton_ClearLatLongElev_clicked()
{
    ui->lineEdit_Lat->setText("");
    ui->lineEdit_Long->setText("");
    ui->lineEdit_Elev->setText("");
}


void Widget::on_pushButton_ClearXYZ_clicked()
{
    ui->lineEdit_X->setText("");
    ui->lineEdit_Y->setText("");
    ui->lineEdit_Z->setText("");
}


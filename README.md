# Latitude Longitude Elevation - X Y Z

Qt C++ wrapper to geodesy routines, in JavaScript, by James R. Clynch, Naval Postgraduate School / 2003.
This project demonstrates how to embed JavaScript code into a Qt C++ application.
